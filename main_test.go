package main

import (
	"testing"
)

// assertEquals is a basic assertEquals implementation
func assertEquals(t *testing.T, foo interface{}, bar interface{}) {
	if foo != bar {
		t.Errorf("%v != %v", foo, bar)
	}
}

// TestFifthElementFinder is our "happy path" test with 10 nodes; ensuring that
// node 5 is returned.
func TestFifthElementFinder(t *testing.T) {
	test1 := CreateLinkedLst("test1")

	test1.Add(1)
	test1.Add(2)
	test1.Add(3)
	test1.Add(4)
	test1.Add(5)
	test1.Add(6)
	test1.Add(7)
	test1.Add(8)
	test1.Add(9)
	test1.Add(10)

	node, err := test1.FifthElementFinder()
	if err != nil {
		t.Errorf("error when determining fifth element from end")
	}
	assertEquals(t, node.data, 6)
}

// TestFifthElementFinderLessThanFive tests that we get an error when the list
// is not, at least, 5 nodes long
func TestFifthElementFinderLessThanFive(t *testing.T) {
	test1 := CreateLinkedLst("test1")

	test1.Add(1)
	test1.Add(2)
	test1.Add(3)
	test1.Add(4)

	node, err := test1.FifthElementFinder()
	// if error doesn't exist, then error out of the test
	if err == nil {
		t.Errorf("%v", node.data)
	}
	// otherwise return pass - string comparison in logic tests is usually avoided
	assertEquals(t, "true", "true")
}

// TestFifthElementFinderExactlySixNodes tests when the list container 6 nodes
// which should return the first node
func TestFifthElementFinderExactlySixNodes(t *testing.T) {
	test1 := CreateLinkedLst("test1")

	test1.Add(1)
	test1.Add(2)
	test1.Add(3)
	test1.Add(4)
	test1.Add(5)
	test1.Add(6)

	node, err := test1.FifthElementFinder()
	if err != nil {
		t.Errorf("error when determining fifth element from end")
	}
	assertEquals(t, node.data, 2)
}

// TestFifthElementFinderExactlyFive tests that we get an error when the list
// is exactly 5 nodes long
func TestFifthElementFinderExactlyFive(t *testing.T) {
	test1 := CreateLinkedLst("test1")

	test1.Add(1)
	test1.Add(2)
	test1.Add(3)
	test1.Add(4)
	test1.Add(5)

	node, err := test1.FifthElementFinder()
	if err != nil {
		t.Errorf("error when determining fifth element from end")
	}
	// otherwise return pass - string comparison in logic tests is usually avoided
	assertEquals(t, node.data, 1)
}
