## 1.0.4 [06-11-2019]
* Bug fixes and performance improvements

See commit ff6d6b8

---

## 1.0.3 [06-06-2019]
* Bug fixes and performance improvements

See commit a8f4df1

---

## 1.0.2 [06-06-2019]
* Bug fixes and performance improvements

See commit 3a069ec

---

## 1.0.1 [06-06-2019]
* Bug fixes and performance improvements

See commit d6ed3e3

---
