package main

import (
	"errors"
	"fmt"
)

// Node represents a singlely linked list node which has reference to only its
// data and the next node
type Node struct {
	next *Node
	data int
}

// LinkedList is our implementation of a singlely linked list
type LinkedList struct {
	name string
	head *Node
	tail *Node
}

// CreateLinkedLst creates the LinkedList
func CreateLinkedLst(name string) *LinkedList {
	return &LinkedList{
		name: name,
	}
}

// Add adds nodes to the linked list
func (L *LinkedList) Add(data int) {
	n := &Node{
		data: data,
	}
	// if the head of the list is nil
	if L.head == nil {
		// add this node as the head
		L.head = n
	} else {
		// else, set the list head to currNode
		currNode := L.head

		// while the next node is not nil, the current node is equal to the
		// next node
		for currNode.next != nil {
			currNode = currNode.next
		}

		// finally, when we've reached the end of the list, let the last node
		// of the list to the one we've passed in (adding it to the end)
		currNode.next = n
	}
}

// FifthElementFinder find the 5th element from the end of the list
func (L *LinkedList) FifthElementFinder() (*Node, error) {
	// set our two pointers to the head of the list
	ptr1 := L.head
	ptr2 := L.head

	// loop the first pointer until it's at index 5
	for i := 0; i < 4; i++ {
		// incremement (better nil checking requires this line before next -- such
		// as the case where the number of nodes is exactly 5
		ptr1 = ptr1.next
		// if there is not a next node before we've reached 5, the list is too short
		if ptr1 == nil {
			return ptr1, errors.New("list is shorter than 5 nodes")
		}
	}

	// while the next element is not nil, begin to iteratively increment our
	// two pointers (at a 5 node distance from one another)
	for ptr1.next != nil {
		ptr1 = ptr1.next
		ptr2 = ptr2.next
	}
	// once our first pointer hits a nil (the end of the list), we return the data
	// from out second pointer
	fmt.Println(string(ptr2.data))
	return ptr2, nil
}

func main() {
	fmt.Println("Fifth Element Finder: v1")
}
