![](https://gitlab.com/jstone28/fifth-element/uploads/275e01f69fab0fd60496cfbd8f6bd2cb/Screen_Shot_2019-06-06_at_10.40.06_AM.png)

# fifth-element

The fifth-element exercise returns the 5th element from the end of a singlely linked list such that the list is not traversed more than once, and list functions are not implemented

## Getting Started

To run this application, clone the project and run:

`go test -v .`

That will kick off a set of tests that verify the functionality of `FifthElementFinder()`

*An output of this command can also be found in the pipelines (CI/CD on the left side navigation bar)*

## Problem

**For a single-linked (forward only) list write a function that returns 5th element from the end of the list. The list can only be walked once (reverse, length, or size of the list cannot be used)**

## Solution Overview

In a singlely linked list like this:

```text
1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7 -> 8 -> 9 -> 10 -> null
```

we can use two pointers to find the 5th element traversing the list only once (using both a for-loop and a while-loop)

Visually, it looks like this:

```text
1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7 -> 8 -> 9 -> 10 -> null

ptr1 = 1
prt2 = 1
```

for i is less than 5 (the interval we want from the end), move ptr1 forward

```text
1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7 -> 8 -> 9 -> 10 -> null
               ^ptr1

```

next, while ptr1.next isn't null (meaning we've gone off the edge of the list), we move ptr2

```text
1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7 -> 8 -> 9 -> 10 -> null
                                              ^ptr1
                    ^ptr2
```

Because of the interval we've set, once ptr1 sees that the next node is null (indicating the end of the list), ptr2 is stopped, and because it's (in our case) 5 nodes from the end of the list, it's the answer we're looking for.

*The assumptions made in this solution are expounded upon in the next section*


## Thoughts During Exercise

* The question doesn't specify what type of data is going to exist in the singlely linked list, and to some extent it doesn't matter but for the sake of testing, I'm going to return ints to make it easier to visualize

* I wouldn't usually push directly on master but because this is a fairly straight forward and finite project :sunglasses:
